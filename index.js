// Mathematical Operators
// /*+-

let firstNumber = 12
let secondNumber = 5
let total = 0

// ARITHMETIC OPERATORS
// Addition Operator, responsible for two or more numbers
total = firstNumber + secondNumber
console.log('result of addition operator ' + total)

// Subtraction Operator, responsible for subtracting two or more numbers
total = firstNumber - secondNumber
console.log('result of subtraction operator ' + total)

// Multiplication Operator, responsible for multiplying two or more numbers
total = firstNumber * secondNumber
console.log('result of multiplication operator ' + total)

// Division Operator, responsible for dividing two or more numbers
total = firstNumber / secondNumber
console.log('result of division operator ' + total)

// Modulo Operator, responsible for getting the remainder of two or more numbers.
// result is 2 of 12 % 5). there is two 5 in 12
total = firstNumber % secondNumber
console.log('result of modulo operator ' + total)


// ASSIGNMENT OPERATORS (=)
// Reassignment operator, should be a single = sign, signifies re-assignment or new value into existing variable
total = 27
console.log(total)

total += 3
// total = total + 3
console.log('result of addition assignment operator is ' + total)

// 
total -= 5
// total = total - 5
console.log('result of subtraction assignment operator is ' + total)

total *= 4
// total = total * 4
console.log('result of multiplication assignment operator is ' + total)

total /= 20
// total = total / 20
console.log('result of Division assignment operator is ' + total)


// MULTIPLE OPERATORS
let mdastotal = 0
let pemdasTotal = 0
let pemdasTotal2 = 0

// program MDAS Rules is followed when doing the multiple operations
mdastotal = 2 + 1 - 5 * 4 / 1
console.log('mdas result is ' + mdastotal)

// PEMDAS Rules
pemdasTotal = 5 + (10-2) / 2 * 3
console.log('pemdas resulu is ' + pemdasTotal)

// for EXPONENT operator declares by using double asterisks ("**") after the number
pemdasTotal2 = 5^2 + (10-2) / 2 * 3
console.log('exponent result is ' + pemdasTotal2)


// INCREMENT AND DECREMENT
let incrementNum = 1
let decrementNum = 5
console.log('pre result return original value: ' + incrementNum)
console.log('pre result return current value: ' + ++incrementNum)
console.log('post result return current value: ' + incrementNum)

// post
console.log('pre result return current value: ' + ++incrementNum)

//  Pre-increment
let preincrementNum1 = ++incrementNum
let predecrementNum1 = --decrementNum


console.log('pre-incrementNum1 result is ' + preincrementNum1)
console.log('pre-decrementNum1 result is ' + predecrementNum1)

// post-increment
let postIncrementNum1 = incrementNum++
let postDecrementNum1 = decrementNum--
console.log('post-incrementNum1 result is ' + postIncrementNum1)
console.log('post-decrementNum1 result is ' + postDecrementNum1)


// COERCION (pinagdidigit)- when you add a 'string' value t a non-string
let a = '10'
let b = 10

console.log('Coercion result is ')
console.log(a + b)

// NON-COERCION (they add together)- when you add 2 or more non-string value
let c = 10
let d = 10

console.log('Non-Coercion result is ')
console.log(c + d)

let stringType = 'Hello'
let numberType = 1
let booleanType = false
let arrayType = ['1', '2', '3']
let objectType = {
    objectKey: 'object value'
}
console.log(typeof stringType)
console.log(typeof numberType)
console.log(typeof booleanType)
console.log(typeof arrayType)
console.log(typeof objectType)


// Computer reads  "true" as 1
// Computer reads "false" as 0
console.log(true + 1)
console.log(false + 1)


// when comparing we use (==)
console.log(5 == 5)
console.log('hello' == 'hello')


// Not strict == (console result true)
console.log(2 == '2')
// strict === (console result false)
console.log(2 === '2')
console.log(true === 1)


// INEQUALITY OPERATOR
// ! is opposite (if true it will become false vice versa)
console.log(5 != '5')
console.log('hello' != 'hello')
console.log(5 != 5)

// STRICT INEQUALITY OPERATOR
// counter part of === as strict in opposite
console.log(1 !== '1')


// RELATIONAL OPERATIONS
let firstVal = 10
let secondVal = 5

console.log(firstVal > secondVal)
console.log(firstVal < secondVal)

// is firstVal mas malaki ba or equal kay secondVal? (console result - true)
console.log(firstVal >= secondVal)
// // is firstVal mas maliit ba or equal kay secondVal? (console result - false)
console.log(firstVal <= secondVal)

// LOGICAL OPERATORS
let isLegalAge = true
let isRegistered = false

// AND Operator - chine'check if true pag may isang false (false na result)
// result console (result)
console.log(isLegalAge && isRegistered)

// OR Operator - once na check na true all true kapag hindi false na lahat
// result console true
console.log(isLegalAge || isRegistered)

// NOT OPERATOR the opposite
// result console (false)
console.log(!isLegalAge)


// TESTING
// result console true
console.log(isLegalAge && !isRegistered)
// result console (false)
console.log(!isLegalAge || isRegistered)


// Truthy and Falsey Values
// [], '', 0 - is false by default
// result console true
console.log('' == false)
console.log([] == false)


// e.g of null case scenario
let userName = null;
let id = 1;

if (id == 1) {
    userName = "ernest";
} else {
    userName = "chen";
}
console.log("user name is: " + userName);

// prompt alert
let age = prompt ('My Age is');

if (age >= 18) {
    alert ("Congrats. Legal age ka na!")
} else {
    alert ("Di ka pa Legal age beh!")
}